<?php

declare(strict_types=1);

namespace Dexodus\AdminConstructorBundle\Dto;

interface PageInterface
{
    public function getType(): string;
}
