<?php

declare(strict_types=1);

namespace Dexodus\AdminConstructorBundle\Service;

use Dexodus\AdminConstructorBundle\Dto\NavigationInterface;
use Dexodus\AdminConstructorBundle\Dto\PageInterface;
use Dexodus\AdminConstructorBundle\PageProcessor\PageProcessorInterface;
use Dexodus\TitleBundle\Attribute\Title;
use Dexodus\TranslationApiBundle\Service\TranslateService;
use Dexodus\TranslationApiBundle\Service\TranslationCompleter;
use Exception;
use ReflectionClass;
use ReflectionProperty;
use Twig\Environment;

class NavigationManager
{
    protected NavigationInterface $navigation;
    /** @var array<string, PageProcessorInterface> */
    protected array $pageProcessors;

    public function __construct(
        private TranslationCompleter $translationCompleter,
        private TranslateService $translateService,
        private Environment $twig,
    ) {
    }

    public function setNavigation(string $navigation): void
    {
        $this->navigation = unserialize($navigation);
    }

    public function setPageProcessors(array $pageProcessors): void
    {
        $this->pageProcessors = $pageProcessors;
    }

    public function getNavigation(): NavigationInterface
    {
        return $this->processNavigation($this->navigation, 'navigation');
    }

    protected function processNavigation(NavigationInterface $navigation, string $context): NavigationInterface
    {
        $navigationReflection = new ReflectionClass($navigation);

        foreach ($navigationReflection->getProperties() as $property) {
            $propertyValue = $property->getValue($navigation);
            $classImplements = class_exists($propertyValue::class) ? class_implements($propertyValue) : [];
            $translationKey = "$context.{$property->getName()}";
            $titleAttribute = $this->getTitleAttribute($property);

            if ($titleAttribute instanceof Title) {
                $titleText = $this->twig->render('@AdminConstructor/string_render.txt.twig', [
                    'string' => $titleAttribute->value,
                    'parentKey' => $context,
                    'parentTitle' => $this->translateService->translate($context, 'ru'),
                ]);
                $this->translationCompleter->complete('ru', [$translationKey => str_replace(PHP_EOL, '', $titleText)]);
            }

            if (in_array(NavigationInterface::class, $classImplements)) {
                $propertyValue = $this->processNavigation($propertyValue, $translationKey);
            }

            if (in_array(PageInterface::class, $classImplements)) {
                if (array_key_exists($propertyValue::class, $this->pageProcessors)) {
                    $propertyValue = $this->pageProcessors[$propertyValue::class]->processPage($propertyValue);
                }
            }

            $property->setValue($navigation, $propertyValue);
        }

        return $navigation;
    }

    private function getTitleAttribute(ReflectionProperty $property): ?Title
    {
        $attributes = $property->getAttributes(Title::class);

        if (count($attributes) > 1) {
            $titleClass = Title::class;

            throw new Exception(
                "In property '{$property->class}::{$property->getName()}'} founded more than one attributes, that implement '$titleClass'",
            );
        }

        if (empty($attributes)) {
            return null;
        }

        return $attributes[0]->newInstance();
    }
}
