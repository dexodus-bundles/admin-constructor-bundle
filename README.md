# AdminConstructorBundle

[Packagist](https://packagist.org/packages/dexodus/admin-constructor-bundle)

1. Install bundle
```shell
composer require dexodus/admin-constructor-bundle:^1.0.0
```

2. Configure bundle
```yaml
# admin_constructor.yaml
admin_constructor:
  navigation_class: App\Admin\Navigation
```
